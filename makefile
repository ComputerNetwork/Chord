all:
	go build dht_peer.go
	go build dht_client.go

#build client
dht_client: dht_client.go
	go build dht_client.go

#build Peer
dht_peer: dht_peer.go
	go build dht_peer.go

#Run Peer/roots - just for debugging
root: dht_peer
	./dht_peer -m 1 -h captain.ccs.neu.edu -p 15115

peer1: dht_peer
	./dht_peer -h=top.ccs.neu.edu -p=15116 -r=15115 -R=captain.ccs.neu.edu

peer2: dht_peer
	./dht_peer -h=tam.ccs.neu.edu -p=15117 -r=15115 -R=captain.ccs.neu.edu

peer3: dht_peer
	./dht_peer -h=turban.ccs.neu.edu -p=15118 -r=15115 -R=captain.ccs.neu.edu

peer4: dht_peer
	./dht_peer -h=fez.ccs.neu.edu -p=15119 -r=15115 -R=captain.ccs.neu.edu

peer5: dht_peer
	./dht_peer -h=beret.ccs.neu.edu -p=15120 -r=15115 -R=captain.ccs.neu.edu

client: dht_client
	./dht_client -r=15115 -R=captain.ccs.neu.edu -h=localhost -p=15121

clean: 
	rm -rf dht_client dht_peer
