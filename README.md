# Chord based Distributed Decentralized Storage System
### Introduction
We are building a Distributed Decentralized Stroage System based on the Chord
protocol. More details on Chord protocol: http://nms.lcs.mit.edu/papers/chord.pdf

### Requirements
- This system contains peers/nodes that form a ring structure. One of the node becomes root.
- Nodes must be able to Join/Leave or Crash anytime and the ring structure should adapt accordingly
- Client accesses the root node to store/retrieve files (Using either recursive/Iterative method) 
- When a node joins, it must check with its neighbors and the files must be redistributed accordingly

### Design & Implementation
#### Peer
Each Peer Class will store following data with it.
```
type Peer struct {
	PeerID uint32 //PeerID: 32bit Integer
	Port string //Port
	Hostname string //IPAddress/Hostname
	PeerEP string //EndPoint: http://192.168.0.101:8080/
	IndexArr []string //IndexArr to store Keys
	RootEP string //EndPoint of Root
	Predecessor Neighbor //Predecessor
	Successor Neighbor //Successor		
	IsRoot bool //Root or Peer
}
```
Each node will expose set of HTTP REST APIs (See details below) to communicate between them. 
Root node starts first. It computes the HashID and creates an array which will store the set of filenames that reside with it.
Initially the Predecessor and Successor will be empty. 
##### Node Joining
- When a new node joins, It must send a /join request to root with its own EndPoint and HashID as Neighbor Struct.
- If the node's predecessor and successor are empty, it adds them as both succ and pred. 
- Else, it checks the new node's HashID with its own HashID. 
- If the node's hash is > current node's hash and < successor's Hash , it informs the new node to forward the request to next node. 
- If the node's hash is < current node's hash and > predecessor's Hash, it informs the new node to forwards the data to prev node.
- Otherwise, it adds the current node as its neighbor. (Predecessor/successor according to hash value.) and updates new node's neighbors by sending details to /addsuccessor and /addpredecessor endpoint.

```
 if p.PeerID < node.HashID {
    	if node.HashID < p.Successor.HashID || p.PeerID > p.Successor.HashID {
    		//New node is the next node
	    	replyJoinSuccess(node, p, 1, rw)
    	} else {
    		//Forward to successor if it is not root.
    		replyJoinRedirect(node, p.Successor, p, rw)
    	}    		
    } else {    	
    	if node.HashID > p.Predecessor.HashID || p.PeerID < p.Predecessor.HashID {
    		//New node is the prev node
	    	replyJoinSuccess(node, p, -1, rw)
    	} else {
    		//Forward to predecessor if its not root
    		replyJoinRedirect(node, p.Predecessor, p, rw)
    	}    		
    }
```
Ex. 
Let's say the ring has 5, 10, 15, 20;
11 is added after 5;
25 is added at the end;
17 is added after 15;
2 is added before 5;
Now the new node must get keys from its neighbor. After getting successful response in joining the ring, 
the node sends exchange key request by calling /exchkeys endpoint on its neighbors. They pick the keys that to be 
moved to the new node based on its hash value and return them as list of keys. Now the new node must send get request
to the nodes and update the keys.
Ex.
Example, Ring: 5, 10, 20, 40. node 20 has keys 12, 14, 16, 17, 19.
If node 18 joins, It gets keys 12, 14, 16, 17.
If node 15 joins, it gets 12, 14
If node 11 joins, it gets None of the keys.

We also handle joining after the last node(With max HashID value among the ring) and before the first node (with least HashID value). 
There we use special conditions to decide where to store the node.

##### Storing File
- When Storing File, the logic is similar as above. Instead of joining the node, we store the file in that node.
- Similarly for handling last node/first node we use special conditions.
Example. Ring: 5, 10, 20, 40. 
Node 5 will have keys from 0-5.
Node 20 will have keys from 10-20 (Ex. 12, 14, 16, 17, 19);
Node 10 will have keys from 5-10;
Node 40 will have all the keys the don't fit into any of these. (>40)

##### Retrieval - Recursive
- Client contacts root and root calls the next node and this goes on till the corresponding node where the data resides is found.
- In this method, each node sends a request to its neighbor and sends the response to its caller. 
- Client calls the root only once and gets the final response.
- After this, client can directly contact the node to receive the object.

##### Retrieval - Iterative
- Client contacts root and receives information about a node.
- Root responds with a node's EndPoint and Status.
- If the response is status 200, then client sends get request to the node
- Else if the response code is 307, then client forwards retrieve request to that node 
- Previous step is repeated till we get 200 Ok response.

##### Get/Put
- Client just calls the /put or /get endpoint on the node where the data has to be put/get.
- If it receives 200 ok, returns successfully
- Else it panics.

##### Printing the ring
- Whenever a new node joins, it sends request to print the ring by calling /print endpoint on root.
- Root will call its successors till the end is found and print the ring structure.
- In our case, we print the endpoints.

#### Hash Functions
We are using SHA1 on the node's EndPoint. This gives us 160bit output. 
We then apply CRC32sum to reduce the key's output further. (32bit)
So we have have 2^32 - 1 nodes in the ring. 

- Hash function is applied on the Node's EndPoint string. Ex. ``` http://crown.ccs.neu.edu:15115/ ```
- For Files, Hash Function is applied on the filename.

#### Crash
- Each node periodically ping its neighbors.
- If a node failure is detected, they call its other active neighbor till they find the end of the ring.
- And then they send add request to that node to make connections and act as neighbor.
- This is like finding the reverse path till we reach the other end of the broken ring.
- We handle only one node failure.
- Ring structure is repaired in this way. But the keys stored are lost. We don't have replicas.

#### HTTP REST API 
We are going to use HTTP REST APIs for communication between nodes. Each node will expose following APIs.
##### POST /join
To ask the root to join the ring.
- Request Type: json
- Request Body: {EndPoint: string, HashID: uint32}
- Response Type: json
- Response Code: 200OK / 307 Temporary Redirect
- Response Body: {Successor: {EndPoint: string, HashID: uint32}, Predecessor: {EndPoint: string, HashID: uint32}}

##### POST /print 
To ask the root to print the ring.
- Request Type: json
- Request Body: {EndPoint: string, HashID: uint32}
- Response Type: json
- Response Code: 200OK 
- Response Body: [{EndPoint: string, HashID: uint32}] (Array of nodes)

##### POST /store
To ask the peer if it can store the data
- Request Type: json
- Request Body: {KeyID: uint32} - Object's HashID
- Response Type: json
- Response Code: 200OK / 307 Temporary Redirect
- Response Body: {EndPoint: string, HashID: uint32}

##### POST /iretrieve
Iterative Retrieve request
- Request Type: json
- Request Body: {KeyID: uint32} - Object's HashID
- Response Type: json
- Response Code: 200OK / 307 Temporary Redirect
- Response Body: {EndPoint: string, HashID: uint32}

##### POST /rretrieve
Recursive Retrieve request
- Request Type: json
- Request Body: {KeyID: uint32} - Object's HashID
- Response Type: json
- Response Code: 200OK / 307 Temporary Redirect
- Response Body: {EndPoint: string, HashID: uint32}

##### GET /hello
To Ping the node using Periodic hello message
- Response Code: 200OK

##### POST /put
To ask the peer to store the data
- Request Type: json
- Request Body: {Key: string, Data: []byte} - (Key is filename; Data is the content)
- Response Type: json
- Response Code: 200OK
- Response Body: NA

##### POST /get
To ask the peer to return the data
- Request Type: json
- Request Body: {Key: string, Data: []byte} - (Key is filename; Data is the empty)
- Response Type: json
- Response Code: 200OK
- Response Body: NA

##### POST /addsuccessor (and) /addpredecessor
To inform the peer about new successor/predecessor
- Request Type: json
- Request Body: {EndPoint: string, HashID: uint32} - New neighor node's details
- Response Code: 200
- Response Body: NA

##### POST /newsuccessor (and) /newpredecessor
To find if the node is the requesting node's new successor. Sent to a peer's neighbor when the other neighbor node crashes.
- Request Type: json
- Request Body: {EndPoint: string, HashID: uint32} - Current node's details
- Response Code: 200
- Response Body: NA

### Configuration and setup

All you need to run this program is the source files. 
make - will build the peer and client programs

As we are using HTTP REST APIs, we must pass proper hostname. Default value is 
localhost but this will make the node not accessible outside local machine.


- Parameters:

```$ ./dht_peer <-m type> <-p own_port <-h own_hostname> <-r root_port> <-R root_hostname>```

- To Start any Normal Node

```$ ./dht_peer -p 3402 -h top.ccs.neu.edu -r 3400 -R crown.ccs.neu.edu```

- To Start Root

```$ ./dht_peer -m 1 -p 3400 -h crown.ccs.neu.edu```

- To Start Client:

```$ ./dht_client <-p client_port> <-h client_hostname> <-r root_port> <-R root_hostname>```

To Debug the program, We expose the following EndPoint.

``` GET /hello ``` 

This will print the following. 
- Node's Successor
- Node's Predecessor
- List of keys stored in the node currently (Just the Hash of filenames)

You can do ``` curl http://crown.ccs.neu.edu:15115/hello ``` and receive this message.

