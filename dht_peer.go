package main
import (
	"fmt"	
	"flag"		
	"log"
	"os"
	"io/ioutil"
	"net/http"
	"hash/crc32"
	"crypto/sha1"
	"encoding/json"
	"bytes"
	"time"
	"strconv"
)
var DBG = 0
type Key struct{
	KeyID uint32 `json:"key_id"`
}

type Data struct{
	Key string `json:"key"`
	Value []byte `json:"value"`
}

type StoreResp struct {
	EndPoint string `json:"endpoint"`
}

type JoinResp struct{
	Successor Neighbor `json:"successor"`
	Predecessor Neighbor `json:"predecessor"`
}

type Neighbor struct{
	HashID uint32 `json:"hash"` //HashID
	EndPoint string `json:"endPoint"` //EndPoint
}

type Hello struct {
	PeerEP string `json:"Peer"` 
	PredEP string `json:"Pred"`
	SuccEP string `json:"Succ"`
	IndexArr []string `json:Index`
}

type Peer struct {
	PeerID uint32 //PeerID: 32bit Integer
	Port string //Port
	Hostname string //IPAddress/Hostname
	PeerEP string //EndPoint: http://192.168.0.101:8080/
	IndexArr []string //IndexArr to store Keys
	RootEP string //EndPoint of Root
	Predecessor Neighbor //Predecessor
	Successor Neighbor //Successor		
	IsRoot bool //Root or Peer
}

func newPeer() *Peer{
	return &Peer{}
}

var hostPtr  = flag.String("h", "localhost", "a string")
var portPtr  = flag.String("p", "15023", "a string")
var rHostPtr = flag.String("R", "localhost", "a string")
var rPortPtr = flag.String("r", "8084", "a string")
var modePtr = flag.Int("m", 0, "an int")

func handleErr(err error){
	if err != nil {
		panic(err)
		os.Exit(-1)
	}
}

func (p *Peer) initialize(){
	p.Hostname = *hostPtr
	p.Port = *portPtr
	m := *modePtr
	p.IsRoot = (m == 1)
	p.PeerEP = "http://" + *hostPtr + ":" + *portPtr + "/"
	if p.IsRoot == true {
		p.RootEP = p.PeerEP
	} else {
		p.RootEP = "http://" + *rHostPtr + ":" + *rPortPtr + "/"
	}
	hash := sha1.Sum([]byte(p.PeerEP))
	p.IndexArr = make([]string, 0)
	p.PeerID = crc32.ChecksumIEEE(hash[0:])
	fmt.Println("Initialized Peer:", p)
}

func helloHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	node := Hello{PeerEP: p.PeerEP, PredEP: p.Predecessor.EndPoint, SuccEP: p.Successor.EndPoint, IndexArr: p.IndexArr}
	rw.WriteHeader(http.StatusOK)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(node)	
}

func newSuccessorHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	if DBG >= 1 {
		fmt.Println("New Successor req Body:", string(body))	    
	}
	var node Neighbor
	err := json.Unmarshal(body, &node)
    fmt.Println("Adding New Successor:", node)
	handleErr(err)
	p.Successor = node
	rw.WriteHeader(http.StatusOK)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(node)
}

func newPredecessorHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	if DBG >= 1 {
		fmt.Println("New Pred. req Body:", string(body))	    
	}
	var node Neighbor
	err := json.Unmarshal(body, &node)
    fmt.Println("Adding New Predecessor:", node)
	handleErr(err)
	p.Predecessor = node
	rw.WriteHeader(http.StatusOK)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(node)
}

func replyJoinRedirect(node Neighbor, dest Neighbor, p *Peer, rw http.ResponseWriter){
	if dest.EndPoint == p.RootEP {
		//Possibly going in loop. Stop. Shouldn't happen
		fmt.Println("Join going in loop")
		os.Exit(-1)
	}
	//Send Response as new node to be contacted with code 307
	rw.WriteHeader(http.StatusTemporaryRedirect)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(dest)
}

func replyJoinSuccess(node Neighbor, p *Peer, pos int, rw http.ResponseWriter){
	client := &http.Client{}
	var jResp JoinResp
	var endPoint string
	currNode := Neighbor {HashID: p.PeerID, EndPoint: p.PeerEP}
	if pos == 1 {
		//new node is next to curr node.
		nextNode := p.Successor
		p.Successor = node
		//Call newpredecessor on nextNode.
		endPoint = nextNode.EndPoint + "newpredecessor"		
		//Send response to node. (succ = nextNode, pre = p)
		jResp = JoinResp {Successor: nextNode, Predecessor: currNode}
		
	} else {
		//new node is prev to curr node.
		prevNode := p.Predecessor
		p.Predecessor = node
		//Call newsuccessor on prevNode.
		endPoint = prevNode.EndPoint + "newsuccessor"
		//Send response to node. (succ = p, pre = prevNode)
		jResp = JoinResp {Successor: currNode, Predecessor: prevNode}
	}
	//Send Update request to prev/next node
	jsonValue, _ := json.Marshal(node)
	if DBG >= 1 {
		fmt.Println("Successful Join Response:", string(jsonValue))
	}
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
	handleErr(err)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	handleErr(err)
	if response.StatusCode != http.StatusOK {
		fmt.Println("Update join req Failed... Exiting")
		os.Exit(-1)
	}
	//send response as new succ,pre with code 200
	rw.WriteHeader(http.StatusOK)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(jResp)
}

func joinHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body) 
	var node Neighbor
	err := json.Unmarshal(body, &node)
	if DBG >= 1 {
		fmt.Println("Join req Body & Node:", string(body), node)	   
	}
	handleErr(err)
	//Initial Condition
    if p.Predecessor.EndPoint == "" || p.Successor.EndPoint == "" {
    	p.Predecessor = node
    	p.Successor = node
		currNode := Neighbor {HashID: p.PeerID, EndPoint: p.PeerEP}
		jResp := JoinResp {Successor: currNode, Predecessor: currNode}
		//send response as new succ,pre with code 200
		rw.WriteHeader(http.StatusOK)
		rw.Header().Set("Content-Type", "application/json")
		json.NewEncoder(rw).Encode(jResp)
		return
    }
    if p.PeerID < node.HashID {
    	if node.HashID < p.Successor.HashID || p.PeerID > p.Successor.HashID {
    		//New node is the next node
	    	replyJoinSuccess(node, p, 1, rw)
    	} else {
    		//Forward to successor if it is not root.
    		replyJoinRedirect(node, p.Successor, p, rw)
    	}    		
    } else {    	
    	if node.HashID > p.Predecessor.HashID || p.PeerID < p.Predecessor.HashID {
    		//New node is the prev node
	    	replyJoinSuccess(node, p, -1, rw)
    	} else {
    		//Forward to predecessor if its not root
    		replyJoinRedirect(node, p.Predecessor, p, rw)
    	}    		
    }   
}

func (p *Peer) writeToDisk(data Data){
	err := ioutil.WriteFile(data.Key, data.Value, 0666)
	handleErr(err)
	p.IndexArr = append(p.IndexArr, data.Key)
}

func computeHash(data string) uint32{
	hash := sha1.Sum([]byte(data))
	hashID := crc32.ChecksumIEEE(hash[0:])
	return hashID
}

func putHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	var data Data
	err := json.Unmarshal(body, &data)
	//fmt.Println("Put req Body & data:", string(body), data)	    
	fmt.Println("Put request:", data.Key)	    
	handleErr(err)
	p.writeToDisk(data)
	jResp := StoreResp{EndPoint: p.PeerEP}
	//send response as new succ,pre with code 200
	rw.WriteHeader(http.StatusOK)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(jResp)
	return
}

func (p *Peer) getValueFromIndex(key string) ([]byte, int){
	ret := 0
	var value []byte
	if _, err := os.Stat(key); 
	os.IsNotExist(err) {
	  ret = -1
	  return value, ret
	}
	value, err := ioutil.ReadFile(key)
	handleErr(err)
	return value, ret
}

func getHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	var data Data
	err := json.Unmarshal(body, &data)
	//fmt.Println("Get req Body & data:", string(body), data)	    
	fmt.Println("Get request:", data.Key)	    
	handleErr(err)
	value, ret := p.getValueFromIndex(data.Key)	
	data.Value = value
	if ret != 0 {
		//Send resource not found error
		rw.WriteHeader(http.StatusNotFound)
	} else {
		//send response as new succ,pre with code 200
		rw.WriteHeader(http.StatusOK)
	}
	if DBG >= 1 {
		fmt.Println("Get returning:", data, ret)
	}
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(data)
	return
}

func sendStoreResponse(rw http.ResponseWriter, ep string, p *Peer, code int){
	if ep == p.RootEP {
		//Possibly going in loop. Stop. Store in this node
		fmt.Println("Storing data here")
		code = http.StatusOK
	}
	jResp := StoreResp{EndPoint: ep}
	//send response as new succ,pre with code c
	rw.WriteHeader(code)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(jResp)
}

func sendRetrieveResponse(rw http.ResponseWriter, ep string, p *Peer, code int){
	if ep == p.RootEP {
		//Possibly going in loop. Stop. Store in this node
		fmt.Println("Rec. Retrieve stops here")
		code = http.StatusOK
	}
	jResp := StoreResp{EndPoint: ep}
	//send response as new succ,pre with code c
	rw.WriteHeader(code)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(jResp)
}

func storeHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	var key Key
	err := json.Unmarshal(body, &key)
	fmt.Println("Got Store Request")
	if DBG >= 1 {
		fmt.Println("Store req Body & Key:", string(body), key)	 
	}   	
	handleErr(err)
	//Initial Condition
    if p.Predecessor.EndPoint == "" || p.Successor.EndPoint == "" {
    	//Store here; Return currNode
		sendStoreResponse(rw, p.PeerEP, p, http.StatusOK)
    }	
    if p.PeerID < key.KeyID {
    	if p.PeerID > p.Successor.HashID {
    		//Store here; return currNode
			sendStoreResponse(rw, p.PeerEP, p, http.StatusOK)
    	} else {
    		//Return successor node if it is not root.
    		sendStoreResponse(rw, p.Successor.EndPoint, p, http.StatusTemporaryRedirect)
    	}    		
    } else {    	
    	if key.KeyID > p.Predecessor.HashID || p.PeerID < p.Predecessor.HashID {
			//Store Here;
			sendStoreResponse(rw, p.PeerEP, p, http.StatusOK)
    	} else {
    		//Return predecessor node if its not root
    		sendStoreResponse(rw, p.Successor.EndPoint, p, http.StatusTemporaryRedirect)
    	}    		
    }

}

func sendRRetrireveResponse(rw http.ResponseWriter, ep string, key Key, p *Peer) {
	if ep == p.RootEP {
		//We may go in loop. Just return current EndPoint	
		jResp := StoreResp{EndPoint: p.PeerEP}
		rw.WriteHeader(http.StatusOK)
		rw.Header().Set("Content-Type", "application/json")
		json.NewEncoder(rw).Encode(jResp)
		return
	}
	//Send Request to neighbor
	client := &http.Client{}	
	endPoint := ep + "rretrieve"
	kIDJson, _ := json.Marshal(key)
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(kIDJson))
	handleErr(err)
	response, err := client.Do(request)
	handleErr(err)
	if response.StatusCode != http.StatusOK {
		fmt.Println("RRetrive req Failed...")
	}
	defer response.Body.Close()
	var jResp StoreResp
	body, _ := ioutil.ReadAll(response.Body)
	if DBG >= 1 {
		fmt.Println("RRetrieve response Body:", string(body))	
	}
	err = json.Unmarshal(body, &jResp)
	handleErr(err)
	//forward the response we get to client
	rw.WriteHeader(response.StatusCode)
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(jResp)

}

func recursiveRetrieveHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	var key Key
	err := json.Unmarshal(body, &key)
	fmt.Println("Got Rec. Retrieve Request")
	if DBG >= 1 {
		fmt.Println("Rec Retrieve req Body & key:", string(body), key)	    
	}
	handleErr(err)
	//Initial Condition
    if p.Predecessor.EndPoint == "" || p.Successor.EndPoint == "" {
    	//Key is here; Return currNode
		sendRetrieveResponse(rw, p.PeerEP, p, http.StatusOK)
    }	
    if p.PeerID < key.KeyID {
    	if p.PeerID > p.Successor.HashID {
    		//Key is here; return currNode
			sendRetrieveResponse(rw, p.PeerEP, p, http.StatusOK)
    	} else {
    		//Return successor node if it is not root.
    		sendRRetrireveResponse(rw, p.Successor.EndPoint, key, p)
    	}    		
    } else {    	
    	if key.KeyID > p.Predecessor.HashID || p.PeerID < p.Predecessor.HashID {
			//Key is Here;
			sendRetrieveResponse(rw, p.PeerEP, p, http.StatusOK)
    	} else {
    		//Return predecessor node if its not root
    		sendRRetrireveResponse(rw, p.Successor.EndPoint, key, p)
    	}    		
    }
}

func leaveHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    fmt.Println("Leaving the ring...")			    
	client := &http.Client{}

	//Initial Condition
    if p.Predecessor.EndPoint == "" || p.Successor.EndPoint == "" {
		//Just leave...
		fmt.Println("Leaving Empties the ring...")
    } else if p.Predecessor == p.Successor {
		//call update predecessor & successor with Empty value			
		nextNode := p.Successor
		node := Neighbor{HashID: 0, EndPoint : ""}
		endPoint := nextNode.EndPoint + "newpredecessor"		
		jsonValue, _ := json.Marshal(node)		
		request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err := client.Do(request)
		handleErr(err)
		if response.StatusCode != http.StatusOK {
			fmt.Println("Update pred. req Failed... Exiting")
			rw.WriteHeader(response.StatusCode)
			rw.Header().Set("Content-Type", "application/json")		
			os.Exit(-1)
		}
		endPoint = nextNode.EndPoint + "newsuccessor"		
		jsonValue, _ = json.Marshal(node)		
		request, err = http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err = client.Do(request)
		handleErr(err)
		if response.StatusCode != http.StatusOK {
			fmt.Println("Update succ. req Failed... Exiting")
			rw.WriteHeader(response.StatusCode)
			rw.Header().Set("Content-Type", "application/json")		
			os.Exit(-1)
		}

    } else {	
		//call update predecessor on p.Successor
		nextNode := p.Successor
		prevNode := p.Predecessor
		endPoint := nextNode.EndPoint + "newpredecessor"		
		jsonValue, _ := json.Marshal(prevNode)		
		request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err := client.Do(request)
		handleErr(err)
		if response.StatusCode != http.StatusOK {
			fmt.Println("Update predecessor req Failed... Exiting")
			rw.WriteHeader(response.StatusCode)
			rw.Header().Set("Content-Type", "application/json")		
			os.Exit(-1)
		}
		//call update successor on p.Predecessor
		endPoint = prevNode.EndPoint + "newsuccessor"		
		jsonValue, _ = json.Marshal(nextNode)		
		request, err = http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err = client.Do(request)
		handleErr(err)
		if response.StatusCode != http.StatusOK {
			fmt.Println("Update successor req Failed... Exiting")
			rw.WriteHeader(response.StatusCode)
			rw.Header().Set("Content-Type", "application/json")		
			os.Exit(-1)
		}
    
    }
    rw.WriteHeader(http.StatusOK)
	rw.Header().Set("Content-Type", "application/json")				

}

func iterativeRetrieveHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	var key Key
	err := json.Unmarshal(body, &key)
	fmt.Println("Got Iterative Retrieve Req")
	if DBG >= 1 {
		fmt.Println("Itr Retrieve req Body & key:", string(body), key)	    
	}
	handleErr(err)
	//Initial Condition
    if p.Predecessor.EndPoint == "" || p.Successor.EndPoint == "" {
    	//Key is here; Return currNode
		sendRetrieveResponse(rw, p.PeerEP, p, http.StatusOK)
    }	
    if p.PeerID < key.KeyID {
    	if p.PeerID > p.Successor.HashID {
    		//Key is here; return currNode
			sendRetrieveResponse(rw, p.PeerEP, p, http.StatusOK)
    	} else {
    		//Return successor node if it is not root.
    		sendRetrieveResponse(rw, p.Successor.EndPoint, p, http.StatusTemporaryRedirect)
    	}    		
    } else {    	
    	if key.KeyID > p.Predecessor.HashID || p.PeerID < p.Predecessor.HashID {
			//Key is Here;
			sendRetrieveResponse(rw, p.PeerEP, p, http.StatusOK)
    	} else {
    		//Return predecessor node if its not root
    		sendRetrieveResponse(rw, p.Successor.EndPoint, p, http.StatusTemporaryRedirect)
    	}    		
    }
}

func printHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	var nodes []Neighbor
	if p.IsRoot == true {
		client := &http.Client{}
		node := p.Successor
		nodes = append(nodes, node)		
		for {
			if node.EndPoint == p.PeerEP {
				fmt.Println("Ring Structure:", nodes)
				break
			}
			endPoint := node.EndPoint + "print"
			request, err := http.NewRequest("GET", endPoint, nil)
			handleErr(err)
			response, err := client.Do(request)
			handleErr(err)
			if response.StatusCode != http.StatusOK {
				fmt.Println("Print req Failed... Exiting")
				os.Exit(-1)	
			}
			defer response.Body.Close()
			var n Neighbor
			body, _ := ioutil.ReadAll(response.Body)			
			err = json.Unmarshal(body, &n)
			handleErr(err)
			nodes = append(nodes, n)
			node = n
		}
		rw.WriteHeader(http.StatusOK)
		rw.Header().Set("Content-Type", "application/json")				
		
	} else {		
		fmt.Println("Got Print Req. Responding:")
		jResp := p.Successor
		rw.WriteHeader(http.StatusOK)
		rw.Header().Set("Content-Type", "application/json")
		json.NewEncoder(rw).Encode(jResp)
	}
}

func (p *Peer) makeGetRequest(key string, endP string) { 
	data := Data {Key: key, Value: []byte("")}
	dataJson,_ := json.Marshal(data)
	client := &http.Client{}		
	endPoint := endP + "get"	
	fmt.Println("Sending Get Req:", string(dataJson))
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(dataJson))
	handleErr(err)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	handleErr(err)
	defer response.Body.Close()							
	if response.StatusCode != http.StatusOK {
		fmt.Println("Unable to get data.", response.StatusCode)	
		return	
	}
	//Print the response
	var respData Data
	body, _ := ioutil.ReadAll(response.Body)
	err = json.Unmarshal(body, &respData)
	handleErr(err)			
	fmt.Println("Got successful response.. Stroing at file:", respData.Key)	
	err = ioutil.WriteFile(respData.Key, respData.Value, 0644)
	handleErr(err)

}

func (p *Peer) addKeysFromNeighbor(response *http.Response, endP string) {
	defer response.Body.Close()
	var keys []Key
	body, _ := ioutil.ReadAll(response.Body)
	err := json.Unmarshal(body, &keys)
	handleErr(err)
	for _, key := range keys {
		keyStr := fmt.Sprint(key.KeyID)
		p.IndexArr = append(p.IndexArr, keyStr)
		//make get request for this key
		p.makeGetRequest(keyStr, endP)
	}
}

func (p *Peer) updateKeys(){
	//call exchkeys on Predecessor	
	client := &http.Client{}	
	node := StoreResp{EndPoint: p.PeerEP}	
	endPoint := p.Predecessor.EndPoint + "exchkeys"		
	jsonValue, err := json.Marshal(node)
	handleErr(err)
	if DBG >= 1 {
		fmt.Println("Sending exchkeys Req to predecessor:", string(jsonValue))	
	}
	fmt.Println("Sending ExchangeKeys Req To Predecessor:", p.Predecessor.EndPoint)
	
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
	handleErr(err)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	handleErr(err)
	if response.StatusCode == http.StatusOK {
		//parse json value and add it to index file.
		p.addKeysFromNeighbor(response, p.Predecessor.EndPoint)
		fmt.Println("Updated Keys from Predecessor")
	} else if response.StatusCode == http.StatusNotFound {
		fmt.Println("No Keys to be added")
	} else {
		fmt.Println("Error receiving response from predecessor")
		os.Exit(-1)
	}
	if p.Successor.EndPoint == p.Predecessor.EndPoint {
		fmt.Println("Exchange done");
		return
	}
	//call exchkeys on Successor
	endPoint = p.Successor.EndPoint	+ "exchkeys"		
	jsonValue, _ = json.Marshal(node)
	if DBG >= 1 {
		fmt.Println("Sending exchkeys Req to successor:", string(jsonValue))
	}
	fmt.Println("Sending ExchangeKeys Req To successor:", p.Successor.EndPoint)
	request, err = http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
	handleErr(err)
	request.Header.Set("Content-Type", "application/json")
	response, err = client.Do(request)
	handleErr(err)
	if response.StatusCode == http.StatusOK {
		//parse json value and add it to index file.
		p.addKeysFromNeighbor(response, p.Successor.EndPoint)	
		fmt.Println("Updated Keys from successor")		
	} else if response.StatusCode == http.StatusNotFound {
		fmt.Println("No Keys to be added")
	} else {
		fmt.Println("Error receiving response from predecessor")
		os.Exit(-1)
	}
}

func exchKeysHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	var node StoreResp
	err := json.Unmarshal(body, &node)
	fmt.Println("Got EXCHKeys req:", node)	    
	handleErr(err)
	var keyList []Key
	neighborID := computeHash(node.EndPoint)
	if neighborID > p.PeerID {
		//Nothing to be transferred.
		rw.WriteHeader(http.StatusNotFound)		
	} else {
		rw.WriteHeader(http.StatusOK)
		//Check all keys in IndexArr
		for i, keyHash := range p.IndexArr {
			kID, err := strconv.ParseUint(keyHash, 10, 32)
			keyID := uint32(kID)
			keyObj := Key{KeyID: keyID}
			handleErr(err)
			if keyID < neighborID {
				keyList = append(keyList, keyObj)
				p.IndexArr = append(p.IndexArr[:i], p.IndexArr[i+1:]...)				
			}
		}
	}
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(keyList)
}

func addSuccessorHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	fmt.Println("Got addSuccessor request:", string(body))	    
	var node Neighbor
	err := json.Unmarshal(body, &node)
	handleErr(err)
	var retNode Neighbor
	if p.isPredecessorAlive() {
		retNode = p.Predecessor
		rw.WriteHeader(http.StatusTemporaryRedirect)	
	} else {
		retNode = Neighbor{HashID: p.PeerID, EndPoint: p.PeerEP}
		rw.WriteHeader(http.StatusOK)	
	}
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(retNode)
}

func addPredecessorHandler(rw http.ResponseWriter, req *http.Request, p *Peer) {
	defer req.Body.Close()
    body, _ := ioutil.ReadAll(req.Body)
	fmt.Println("Got addPredecessor request:", string(body))	    
	var node Neighbor
	err := json.Unmarshal(body, &node)
	handleErr(err)
	var retNode Neighbor
	if p.isSuccessorAlive() {
		retNode = p.Successor
		rw.WriteHeader(http.StatusTemporaryRedirect)	
	} else {
		retNode = Neighbor{HashID: p.PeerID, EndPoint: p.PeerEP}
		rw.WriteHeader(http.StatusOK)	
	}
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(retNode)
}

func newNeighbor(endPoint string) (Neighbor) {
	var node Neighbor  
	node.HashID = computeHash(endPoint)
	node.EndPoint = endPoint
	return node
}

func (p *Peer) handleSuccessorCrash() {
	client := &http.Client{}	
	node := Neighbor {HashID: p.PeerID, EndPoint: p.PeerEP}
	predEP := p.Predecessor.EndPoint
	endPoint := predEP + "addSuccessor"		
	for {
		jsonValue, _ := json.Marshal(node)
		fmt.Println("Sending addSuccessor Req:", endPoint)
		request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err := client.Do(request)
		handleErr(err)
		if response.StatusCode == http.StatusOK {
			//Add this node from response as successor.
			defer response.Body.Close()		
			var respNode Neighbor
			body, _ := ioutil.ReadAll(response.Body)
			err = json.Unmarshal(body, &respNode)
			handleErr(err)			
			p.Successor = respNode
			return
		} else if response.StatusCode == http.StatusTemporaryRedirect {
			//Forward the request to this response node
			defer response.Body.Close()		
			var respNode Neighbor
			body, _ := ioutil.ReadAll(response.Body)
			err = json.Unmarshal(body, &respNode)
			handleErr(err)
			if respNode.EndPoint == p.PeerEP {
				fmt.Println("Succ Crash handler Going in loop...Probably ring fixed")
				//from EndPoint, create neighbor.
				p.Successor = newNeighbor(predEP)
				return
			}
			predEP = respNode.EndPoint
			endPoint = predEP + "addSuccessor"
		} else {
			fmt.Println("HandleSuccessorCrash failed...", response.StatusCode)
			os.Exit(-1)
		}		
	}	
}

func (p *Peer) handlePredecessorCrash() {
	client := &http.Client{}	
	node := Neighbor {HashID: p.PeerID, EndPoint: p.PeerEP}
	succEP := p.Successor.EndPoint
	endPoint := succEP + "addPredecessor"		
	for {
		jsonValue, _ := json.Marshal(node)
		fmt.Println("Sending addPredecessor Req:", endPoint)
		request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err := client.Do(request)
		handleErr(err)
		if response.StatusCode == http.StatusOK {
			//Add this node from response as successor.
			defer response.Body.Close()		
			var respNode Neighbor
			body, _ := ioutil.ReadAll(response.Body)
			err = json.Unmarshal(body, &respNode)
			handleErr(err)
			p.Predecessor = respNode
			return
		} else if response.StatusCode == http.StatusTemporaryRedirect {
			//Forward the request to this response node
			defer response.Body.Close()		
			var respNode Neighbor
			body, _ := ioutil.ReadAll(response.Body)
			err = json.Unmarshal(body, &respNode)
			handleErr(err)
			if respNode.EndPoint == p.PeerEP {
				fmt.Println("Pred Crash handler Going in loop...Probably ring fixed")				
				//from EndPoint, create neighbor.
				p.Predecessor = newNeighbor(succEP)
				return				
			}
			succEP = respNode.EndPoint
			endPoint = succEP + "addPredecessor"

		} else {
			fmt.Println("handlePredecessorCrash failed...")
			os.Exit(-1)
		}		
	}	
}

func (p *Peer) isSuccessorAlive() (bool) {
	client := &http.Client{}			
	sEndPoint := p.Successor.EndPoint + "hello"	
	sRequest, err := http.NewRequest("GET", sEndPoint, nil)
	handleErr(err)
	_, err = client.Do(sRequest)				
	if err != nil {
		fmt.Println("Successor Died", err)
		return false
	}	
	if DBG >= 1 {	
		fmt.Println("Successor Alive")
	}
	return true
}

func (p *Peer) isPredecessorAlive() (bool) {
	client := &http.Client{}	
	pEndPoint := p.Predecessor.EndPoint + "hello"	
	pRequest, err := http.NewRequest("GET", pEndPoint, nil)
	handleErr(err)
	_, err = client.Do(pRequest)
	if err != nil {
		fmt.Println("Predecessor Died", err)
		return false
	}
	if DBG >= 1 {			
		fmt.Println("Predecessor Alive")
	}
	return true
}

func (p *Peer) pingNeighbors() {
	time.Sleep(15 * time.Second)
	for {
	
		if p.Successor.EndPoint == "" || p.Predecessor.EndPoint == "" {
			time.Sleep(15 * time.Second)
			continue
		}
						
		var isSuccAlive = p.isSuccessorAlive()
		var isPredAlive =  p.isPredecessorAlive() 		
		if isSuccAlive == false && isPredAlive == false {
			if !p.IsRoot {
				fmt.Println("FATAL: Two nodes died/Root Died...")
				os.Exit(-1)
			} else {
				p.Successor = Neighbor{HashID: 0, EndPoint: ""}
				p.Predecessor = Neighbor{HashID: 0, EndPoint: ""}
			}
			
		}
		
		if isSuccAlive == false {
			if p.Successor.EndPoint == p.RootEP {
				fmt.Println("FATAL: Root Died...")
				os.Exit(-1)
			} else if p.Successor != p.Predecessor {
				p.handleSuccessorCrash()
			} else {
				p.Successor = Neighbor{HashID: 0, EndPoint: ""}
				p.Predecessor = Neighbor{HashID: 0, EndPoint: ""}
			}
		}
		if isPredAlive == false {
			if p.Predecessor.EndPoint == p.RootEP {
				fmt.Println("FATAL: Root Died...")
				os.Exit(-1)			
			} else if p.Successor != p.Predecessor {
				p.handlePredecessorCrash()
			} else {
				p.Successor = Neighbor{HashID: 0, EndPoint: ""}
				p.Predecessor = Neighbor{HashID: 0, EndPoint: ""}
			}
		}
		time.Sleep(15 * time.Second)
	}	
}

func (p *Peer) sendPrintRequest(){
	time.Sleep(2 * time.Second)
	fmt.Println("Sending request to Print the ring...")
	client := &http.Client{}			
	endPoint := p.RootEP + "print"	
	request, err := http.NewRequest("GET", endPoint, nil)
	handleErr(err)
	response, err := client.Do(request)
	handleErr(err)
	if response.StatusCode == http.StatusOK {
		fmt.Println("Successfully printed the ring")
	} else {
		fmt.Println("Unable to print the ring")
	}	
}

func (p *Peer) joinRing(){
	if p.IsRoot == true {
		fmt.Println("Starting Root at Port:", p.Port)
	    go p.pingNeighbors()
	} else {
		//Call Join on root.
		fmt.Println("Starting Node at Port:", p.Port)
		node := Neighbor {HashID: p.PeerID, EndPoint: p.PeerEP}
		endPoint := p.RootEP +"join"
		for {
			client := &http.Client{}			
			jsonValue, _ := json.Marshal(node)
			fmt.Println("Sending Join Req:", string(jsonValue))
			request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
			handleErr(err)
			request.Header.Set("Content-Type", "application/json")
			response, err := client.Do(request)
			handleErr(err)
			if response.StatusCode == http.StatusOK {
				//Update pre, succ	
				defer response.Body.Close()		
				var n JoinResp
				body, _ := ioutil.ReadAll(response.Body)
				err = json.Unmarshal(body, &n)
				handleErr(err)
				p.Successor = n.Successor
				p.Predecessor = n.Predecessor
				fmt.Println("Added to the ring successfully", p)
				p.updateKeys()				
			    go p.sendPrintRequest()
			    go p.pingNeighbors()
				break
			} else if response.StatusCode == http.StatusTemporaryRedirect {
				defer response.Body.Close()							
				var n Neighbor
				body, _ := ioutil.ReadAll(response.Body)
				err = json.Unmarshal(body, &n)
				handleErr(err)
				fmt.Println("Need to forward to new server", n)
				endPoint = n.EndPoint + "join"												
			} else {
				fmt.Println("Unknown status code", response.StatusCode)
				os.Exit(-1)
			}
		}

	}
}

func main(){
	flag.Parse()
	p := newPeer()
	p.initialize()
	p.joinRing()
	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		helloHandler(w, r, p)
	})	
	http.HandleFunc("/join", func(w http.ResponseWriter, r *http.Request) {
		joinHandler(w, r, p)
	})
   	http.HandleFunc("/newsuccessor", func(w http.ResponseWriter, r *http.Request) {
		newSuccessorHandler(w, r, p)
	})
	http.HandleFunc("/newpredecessor", func(w http.ResponseWriter, r *http.Request) {
		newPredecessorHandler(w, r, p)
	})
	http.HandleFunc("/print", func(w http.ResponseWriter, r *http.Request) {
		printHandler(w, r, p)
	})
	http.HandleFunc("/store", func(w http.ResponseWriter, r *http.Request) {
		storeHandler(w, r, p)
	})
	http.HandleFunc("/put", func(w http.ResponseWriter, r *http.Request) {
		putHandler(w, r, p)
	})
	http.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
		getHandler(w, r, p)
	})
	http.HandleFunc("/iretrieve", func(w http.ResponseWriter, r *http.Request) {
		iterativeRetrieveHandler(w, r, p)
	})
	http.HandleFunc("/rretrieve", func(w http.ResponseWriter, r *http.Request) {
		recursiveRetrieveHandler(w, r, p)
	})
	http.HandleFunc("/leave", func(w http.ResponseWriter, r *http.Request) {
		leaveHandler(w, r, p)
	})
	http.HandleFunc("/exchkeys", func(w http.ResponseWriter, r *http.Request) {
		exchKeysHandler(w, r, p)
	})
   	http.HandleFunc("/addSuccessor", func(w http.ResponseWriter, r *http.Request) {
		addSuccessorHandler(w, r, p)
	})
	http.HandleFunc("/addPredecessor", func(w http.ResponseWriter, r *http.Request) {
		addPredecessorHandler(w, r, p)
	})
	addrStr := ":" + p.Port
	log.Fatal(http.ListenAndServe(addrStr, nil))
}
