package main
import (
	"fmt"	
	"flag"		
	"os"
	"io/ioutil"
	"net/http"
	"encoding/json"
	"bytes"
	"crypto/sha1"
	"bufio"
	"hash/crc32"
	"strings"
)

type StoreResp struct {
	EndPoint string `json:"endpoint"`
}

type Key struct{
	KeyID uint32 `json:"key_id"`
}

type Data struct{
	Key string `json:"key"`
	Value []byte `json:"value"`
}

type Client struct {	
	Port string //Port
	Hostname string //IPAddress/Hostname
	RootEP string //EndPoint of Root
}

func newClient() *Client{
	return &Client{}
}

var hostPtr  = flag.String("h", "localhost", "a string")
var portPtr  = flag.String("p", "15023", "a string")
var rHostPtr = flag.String("R", "localhost", "a string")
var rPortPtr = flag.String("r", "8084", "a string")

func handleErr(err error){
	if err != nil {
		panic(err)
		os.Exit(-1)
	}
}

func (c *Client) initialize(){
	c.Hostname = *hostPtr
	c.Port = *portPtr
	c.RootEP = "http://" + *rHostPtr + ":" + *rPortPtr + "/"
	fmt.Println("Initialized Client:", c)
}

func (c *Client) put(node StoreResp, dataJson []byte) {
	client := &http.Client{}		
	endPoint := node.EndPoint + "put"	
	fmt.Println("Sending Put Req")
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(dataJson))
	handleErr(err)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	handleErr(err)
	defer response.Body.Close()							
	if response.StatusCode != http.StatusOK {
		fmt.Println("Unable to store data.", response.StatusCode)
	} else {
		fmt.Println("Successfully stored data.", response.StatusCode)
	}
	
}

func (c *Client) get(node StoreResp, dataJson []byte) {
	client := &http.Client{}		
	endPoint := node.EndPoint + "get"	
	fmt.Println("Sending Get Req:", string(dataJson))
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(dataJson))
	handleErr(err)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	handleErr(err)
	defer response.Body.Close()							
	if response.StatusCode != http.StatusOK {
		fmt.Println("Unable to get data.", response.StatusCode)		
		return
	}
	//Print the response
	var data Data
	body, _ := ioutil.ReadAll(response.Body)
	err = json.Unmarshal(body, &data)
	handleErr(err)		
	//fmt.Println(string(data.Value))	
	fmt.Println("Got successful response.. Stroing at file:", data.Key+"_c")
	
	err = ioutil.WriteFile(data.Key+"_c", data.Value, 0644)
	handleErr(err)
}

func (c *Client) iterativeRetrive(){
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter the Key(FileName):")	
	key, _ := reader.ReadString('\n')
	key = strings.TrimSuffix(key, "\n")
	KeyHash := computeHash(key)
	KeyID := Key {KeyID: KeyHash}
	keyStr := fmt.Sprint(KeyHash)
	data := Data {Key: keyStr, Value: []byte("")}
	endPoint := c.RootEP +"iretrieve"
	kIDJson, _ := json.Marshal(KeyID)
	dataJson,_ := json.Marshal(data)
	fmt.Println("Sending itr. Retrieve Req:", string(kIDJson))
	for {
		client := &http.Client{}			
		request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(kIDJson))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err := client.Do(request)
		handleErr(err)
		defer response.Body.Close()							
		var node StoreResp
		body, _ := ioutil.ReadAll(response.Body)
		err = json.Unmarshal(body, &node)
		handleErr(err)		
		if response.StatusCode == http.StatusOK {
			fmt.Println("Node to be Queried:", node)
			c.get(node, dataJson)
			break
		} else if response.StatusCode == http.StatusTemporaryRedirect {
			fmt.Println("Need to forward to new server", node)
			endPoint = node.EndPoint + "iretrieve"												
		} else {
			fmt.Println("Unknown status code", response.StatusCode)
			os.Exit(-1)
		}
	}
}

func (c *Client) recursiveRetrive(){

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter the Key(FileName):")	
	key, _ := reader.ReadString('\n')
	key = strings.TrimSuffix(key, "\n")
	KeyHash := computeHash(key)
	KeyID := Key {KeyID: KeyHash}
	keyStr := fmt.Sprint(KeyHash)
	data := Data {Key: keyStr, Value: []byte("")}
	endPoint := c.RootEP +"rretrieve"
	kIDJson, _ := json.Marshal(KeyID)
	dataJson,_ := json.Marshal(data)
	fmt.Println("Sending rec. Retrieve Req:", string(kIDJson))

	client := &http.Client{}			
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(kIDJson))
	handleErr(err)
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	handleErr(err)
	defer response.Body.Close()							
	var node StoreResp
	body, _ := ioutil.ReadAll(response.Body)
	err = json.Unmarshal(body, &node)
	handleErr(err)		
	if response.StatusCode == http.StatusOK {
		fmt.Println("Node to be Queried:", node)
		c.get(node, dataJson)		
	} else {
		fmt.Println("Error", response.StatusCode)
		os.Exit(-1)
	}
}


func computeHash(data string) uint32{
	hash := sha1.Sum([]byte(data))
	hashID := crc32.ChecksumIEEE(hash[0:])
	return hashID
}

func (c *Client) store(){
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter the Key(FileName):")	
	key, err := reader.ReadString('\n')
	key = strings.TrimSuffix(key, "\n")
	handleErr(err)
	value, err := ioutil.ReadFile(key)
	handleErr(err)
	KeyHash := computeHash(key)
	KeyID := Key {KeyID: KeyHash}
	keyStr := fmt.Sprint(KeyHash)
	data := Data {Key: keyStr, Value: value}
	endPoint := c.RootEP +"store"
	kIDJson, _ := json.Marshal(KeyID)
	dataJson,_ := json.Marshal(data)
	fmt.Println("Sending Store Req:", string(kIDJson))
	for {
		client := &http.Client{}			
		request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(kIDJson))
		handleErr(err)
		request.Header.Set("Content-Type", "application/json")
		response, err := client.Do(request)
		handleErr(err)
		defer response.Body.Close()							
		var node StoreResp
		body, _ := ioutil.ReadAll(response.Body)
		err = json.Unmarshal(body, &node)
		handleErr(err)		
		if response.StatusCode == http.StatusOK {
			fmt.Println("Node to be inserted:", node)
			c.put(node, dataJson)
			break
		} else if response.StatusCode == http.StatusTemporaryRedirect {
			fmt.Println("Need to forward to new server", node)
			endPoint = node.EndPoint + "store"												
		} else {
			fmt.Println("Unknown status code", response.StatusCode)
			os.Exit(-1)
		}
	}

}

func (c *Client) serveMenu(){
	var input string
	for {		
		fmt.Println("Welcome to Chord Client...Choose the options below")
		fmt.Println("-----------------------------------------------------")
		fmt.Println("S : Store a Key-Value Pair")
		fmt.Println("I : Iterative retrieval of object")
		fmt.Println("R : Recursive retrieval of object")
		fmt.Println("E : Exit")
		fmt.Println("-----------------------------------------------------")
		fmt.Scanf("%s\n", &input)		
		if input[0] == 'S' {
			c.store()
		} else if input[0] == 'I' {
			c.iterativeRetrive()
		} else if input[0] == 'R' {
			c.recursiveRetrive()
		} else if input[0] == 'E' {
			os.Exit(0)
		} else {
			fmt.Println("Unknown Option... Please retry")
		}		
	}
}

func main(){
	flag.Parse()
	c := newClient()
	c.initialize()
	c.serveMenu()
}
